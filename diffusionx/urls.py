from django.conf.urls import url

from . import views
from django.views.generic.base import RedirectView

favicon_view = RedirectView.as_view(url='/static/img/favicon.png', permanent=True)

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^login/', views.login_view, name='login_view'),
    url(r'^logout', views.logout_view, name='logout'),
    url(r'^register/', views.register),
    url(r'^experiment/$', views.experiment_list, name='experiment_list'),
    url(r'^experiment/step/1$', views.step1, name='step1'),
    url(r'^experiment/step/2$', views.step2),
    url(r'^experiment/step/3$', views.step3),
    url(r'^experiment/state/1$', views.step1_state),
    url(r'^experiment/state/2$', views.step2_state),
    url(r'^experiment/state/3$', views.step3_state),
    url(r'^experiment/state/submit$', views.run_experiment),
    url(r'^experiment/new$', RedirectView.as_view(url='/experiment/step/1', permanent=False), name='experiment_new'),
    url(r'^experiment/(?P<pk>\d+)$', views.experiment_detail, name='experiment_detail'),
    url(r'^experiment/(?P<pk>\d+)/delete$', views.experiment_delete, name='experiment_delete'),
    url(r'^favicon\.ico$', favicon_view),
    url(r'^network/$', views.network_list, name='network_list'),
    url(r'^network/new$', views.network_new, name='network_new'),
    url(r'^network/(?P<pk>\d+)$', views.network_detail, name='network_detail'),
    url(r'^network/(?P<pk>\d+)/delete$', views.network_delete, name='network_delete'),
    url(r'^get_json_graph/(?P<pk>\d+)$', views.get_json_graph),
    url(r'^graph/(\d+)/$', views.network_graph),
    url(r'^diffusion/(?P<pk>\d+)$', views.diffusion, name='diffusion'),
    url(r'^diffusion/(?P<pk>\d+)/delete$', views.diffusion_delete, name='diffusion_delete'),
    url(r'^get_json_diffusion/(?P<pk>\d+)$', views.get_json_diffusion),
]
