from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from diffusionx.models import Network, Profile, Experiment, Diffusion


class ProfileInline(admin.StackedInline):
    model = Profile
    can_delete = False
    verbose_name_plural = 'Profile'
    fk_name = 'user'


class CustomUserAdmin(UserAdmin):
    inlines = (ProfileInline,)
    list_display = ('username', 'email', 'is_staff', 'is_active', 'get_bio')
    list_filter = ('is_staff', 'date_joined', 'last_login', 'is_active')
    list_select_related = ('profile',)

    def get_bio(self, instance):
        return instance.profile.bio

    get_bio.short_description = 'Bio'

    def get_inline_instances(self, request, obj=None):
        if not obj:
            return list()
        return super(CustomUserAdmin, self).get_inline_instances(request, obj)


admin.site.unregister(User)
admin.site.register(User, CustomUserAdmin)


@admin.register(Network)
class NetworkAdmin(admin.ModelAdmin):
    list_display = ('name', 'user', 'nodes', 'edges', 'added_at', 'directed')
    list_filter = ('user', 'added_at')


class DiffusionInline(admin.TabularInline):
    model = Diffusion
    fields = ['diffusion_id', 'model', 'seed_criteria', 'cover', 'steps', 'network_id']
    readonly_fields = ('diffusion_id', 'network_id',)

    def diffusion_id(self, obj):
        obj_id = obj.pk
        return '<a href="/admin/diffusionx/diffusion/%s"> %s </a>' % (obj_id, obj_id)

    def network_id(self, obj):
        obj_id = obj.network.pk
        return '<a href="/admin/diffusionx/network/%s"> %s </a>' % (obj_id, obj_id)

    diffusion_id.allow_tags = True
    network_id.allow_tags = True
    diffusion_id.short_description = 'Diffusion'
    network_id.short_description = 'NetworkID'


@admin.register(Experiment)
class ExperimentAdmin(admin.ModelAdmin):
    list_display = ('user', 'started_at')
    list_filter = ('user', 'started_at')

    inlines = (DiffusionInline,)


@admin.register(Diffusion)
class DiffusionAdmin(admin.ModelAdmin):
    list_display = ('_model', '_seed', 'cover', 'steps', 'network')

    @staticmethod
    def _model(obj):
        return obj.get_model_display()

    @staticmethod
    def _seed(obj):
        return obj.get_seed_criteria_display()
