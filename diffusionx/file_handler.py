import networkx as nx


def handle_csv_data(csv_file, delimiter=None, comments='#', directed=False, edge_type=None, wgt_name='weight'):
    graph_type = nx.Graph()
    if directed:
        graph_type = nx.DiGraph()

    types = {'none': None, 'int': int, 'float': float, 'string': str}
    wgt_type = types.get(edge_type, None)

    edge_data = None
    if wgt_type:
        edge_data = ((wgt_name, wgt_type),)

    return nx.read_edgelist(csv_file, comments=comments, create_using=graph_type, delimiter=delimiter, data=edge_data)
