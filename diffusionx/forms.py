import networkx as nx
from django import forms
from django.contrib.auth import authenticate


class LoginForm(forms.Form):
    username = forms.CharField(max_length=255, required=True)
    password = forms.CharField(widget=forms.PasswordInput, required=True)

    def login(self, request):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        user = authenticate(username=username, password=password)

        return user


class UserRegistrationForm(forms.Form):
    username = forms.CharField(
        required=True,
        max_length=150,
        widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Username'})
    )
    email = forms.EmailField(
        required=True,
        max_length=254,
        widget=forms.EmailInput(attrs={'class': 'form-control', 'placeholder': 'Email'})
    )
    password = forms.CharField(
        required=True,
        widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Password'})
    )
    bio = forms.CharField(
        required=False,
        max_length=500,
        widget=forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Shortly about you'})
    )


class BarabasiAlbertGraphForm(forms.Form):
    bag_n = forms.IntegerField(
        label='N*',
        required=True,
        min_value=1,
        widget=forms.NumberInput(attrs={'id': 'bag_n', 'class': 'form-control', 'step': '1',
                                        'title': "Number of nodes",
                                        'data-toggle': 'tooltip',
                                        'data-placement': 'bottom'})
    )
    bag_m = forms.IntegerField(
        label='M*',
        required=True,
        min_value=1,
        widget=forms.NumberInput(attrs={'class': 'form-control', 'step': '1',
                                        'title': "Number of edges to attach from a new node to existing nodes",
                                        'data-toggle': 'tooltip',
                                        'data-placement': 'bottom'})
    )
    seed = forms.IntegerField(
        required=False,
        widget=forms.NumberInput(attrs={'class': 'form-control', 'step': '1',
                                        'title': "Seed for random number generator (optional)",
                                        'data-toggle': 'tooltip',
                                        'data-placement': 'bottom'})
    )

    def clean(self):
        form_data = self.cleaned_data
        m = int(form_data['bag_m'])
        n = int(form_data['bag_n'])
        if m < 1 or m >= n:
            raise forms.ValidationError("Conditions m >= 1 and m < n must be met in Barabási–Albert network")
        return form_data

    def get_network(self):
        form_data = self.clean()
        m = form_data.get('bag_m')
        n = form_data.get('bag_n')
        seed = form_data.get('seed')
        graph = nx.barabasi_albert_graph(n, m, seed)
        return graph


class ErdosRenyiGraphForm(forms.Form):
    nodes = forms.IntegerField(
        label='Nodes*',
        required=True,
        min_value=1,
        widget=forms.NumberInput(attrs={'class': 'form-control', 'step': '1',
                                        'title': "Number of nodes",
                                        'data-toggle': 'tooltip',
                                        'data-placement': 'bottom'})
    )
    probability = forms.FloatField(
        label='Probability*',
        required=True,
        min_value=0,
        max_value=1,
        widget=forms.NumberInput(attrs={'class': 'form-control', 'step': '0.01',
                                        'title': "Probability for edge creation",
                                        'data-toggle': 'tooltip',
                                        'data-placement': 'bottom'})
    )
    seed = forms.IntegerField(
        required=False,
        widget=forms.NumberInput(attrs={'class': 'form-control', 'step': '1',
                                        'title': "Seed for random number generator (optional)",
                                        'data-toggle': 'tooltip',
                                        'data-placement': 'bottom'})
    )
    directed = forms.BooleanField(
        required=False,
        widget=forms.CheckboxInput(attrs={'title': "Should a graph be directed? (optional, default: not directed)",
                                          'data-toggle': 'tooltip',
                                          'data-placement': 'bottom'})
    )

    def get_network(self):
        form_data = self.clean()
        n = form_data.get('nodes')
        prob = form_data.get('probability')
        seed = form_data.get('seed')
        directed = form_data.get('directed')
        graph = nx.fast_gnp_random_graph(n, prob, seed=seed, directed=directed)
        return graph


class NewmanWattsStrogatzGraphForm(forms.Form):
    n = forms.IntegerField(
        label='N*',
        required=True,
        min_value=1,
        widget=forms.NumberInput(attrs={'class': 'form-control', 'step': '1',
                                        'title': "The number of nodes",
                                        'data-toggle': 'tooltip',
                                        'data-placement': 'bottom'})
    )
    k = forms.IntegerField(
        label='K*',
        required=True,
        min_value=1,
        widget=forms.NumberInput(attrs={'class': 'form-control', 'step': '1',
                                        'title': "Each node is joined with 'k' nearest neighbors in a ring topology",
                                        'data-toggle': 'tooltip',
                                        'data-placement': 'bottom'})
    )
    probability = forms.FloatField(
        label='Probability*',
        required=True,
        min_value=0,
        max_value=1,
        widget=forms.NumberInput(attrs={'class': 'form-control', 'step': '0.01',
                                        'title': "The probability of adding a new edge for each edge",
                                        'data-toggle': 'tooltip',
                                        'data-placement': 'bottom'})
    )
    seed = forms.IntegerField(
        required=False,
        widget=forms.NumberInput(attrs={'class': 'form-control', 'step': '1',
                                        'title': "The seed for the random number generator (optional)",
                                        'data-toggle': 'tooltip',
                                        'data-placement': 'bottom'})
    )

    def clean(self):
        form_data = self.cleaned_data
        if int(form_data['k']) >= int(form_data['n']):
            raise forms.ValidationError("K must be smaller than N in Newman–Watts–Strogatz graph")
        return form_data

    def get_network(self):
        form_data = self.clean()
        n = form_data.get('n')
        k = form_data.get('k')
        prob = form_data.get('probability')
        seed = form_data.get('seed')
        graph = nx.newman_watts_strogatz_graph(n, k, prob, seed=seed)
        return graph


class WattsStrogatzGraphForm(forms.Form):
    n = forms.IntegerField(
        label='N*',
        required=True,
        min_value=1,
        widget=forms.NumberInput(attrs={'class': 'form-control', 'step': '1',
                                        'title': "The number of nodes",
                                        'data-toggle': 'tooltip',
                                        'data-placement': 'bottom'})
    )
    k = forms.IntegerField(
        label='K*',
        required=True,
        min_value=0,
        widget=forms.NumberInput(attrs={'class': 'form-control', 'step': '1',
                                        'title': "Each node is joined with its 'k' nearest neighbors in a ring topology"
                                        , 'data-toggle': 'tooltip',
                                        'data-placement': 'bottom'})
    )
    probability = forms.FloatField(
        label='Probability*',
        required=True,
        widget=forms.NumberInput(attrs={'class': 'form-control', 'min': 0, 'max': 1, 'step': '0.01',
                                        'title': "Probability of rewiring each edge",
                                        'data-toggle': 'tooltip',
                                        'data-placement': 'bottom'})
    )
    seed = forms.IntegerField(
        required=False,
        widget=forms.NumberInput(attrs={'class': 'form-control', 'step': '1',
                                        'title': "Seed for random number generator (optional)",
                                        'data-toggle': 'tooltip',
                                        'data-placement': 'bottom'})
    )

    def clean(self):
        form_data = self.cleaned_data
        if int(form_data['k']) >= int(form_data['n']):
            raise forms.ValidationError("K must be smaller than N in Watts–Strogatz graph")
        return form_data

    def get_network(self):
        form_data = self.clean()
        n = form_data.get('n')
        k = form_data.get('k')
        prob = form_data.get('probability')
        seed = form_data.get('seed')
        graph = nx.watts_strogatz_graph(n, k, prob, seed=seed)
        return graph


class RandomRegularGraphForm(forms.Form):
    rg_d = forms.IntegerField(
        label='D*',
        required=True,
        min_value=1,
        widget=forms.NumberInput(attrs={'class': 'form-control', 'min': 0, 'step': '1',
                                        'title': "The degree of each node. The value of N×D must be even.",
                                        'data-toggle': 'tooltip',
                                        'data-placement': 'bottom'})
    )
    rg_n = forms.IntegerField(
        label='N*',
        required=True,
        min_value=1,
        widget=forms.NumberInput(attrs={'class': 'form-control', 'min': 0, 'step': '1',
                                        'title': "Number of nodes",
                                        'data-toggle': 'tooltip',
                                        'data-placement': 'bottom'})
    )
    seed = forms.IntegerField(
        required=False,
        widget=forms.NumberInput(attrs={'class': 'form-control', 'step': '1',
                                        'title': "The seed for random number generator",
                                        'data-toggle': 'tooltip',
                                        'data-placement': 'bottom'})
    )

    def clean(self):
        form_data = self.cleaned_data
        n = int(form_data['rg_n'])
        d = int(form_data['rg_d'])

        if (n * d) % 2 != 0:
            raise forms.ValidationError("N * D must be even")

        if not 0 <= d < n:
            raise forms.ValidationError("The 0 <= d < n inequality must be satisfied")

        return form_data

    def get_network(self):
        form_data = self.clean()
        n = form_data.get('rg_n')
        d = form_data.get('rg_d')
        seed = form_data.get('seed')
        graph = nx.random_regular_graph(d, n, seed=seed)
        return graph