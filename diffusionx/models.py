from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.postgres.fields import JSONField
from networkx import is_directed
from networkx.readwrite import json_graph
from django.utils import timezone


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    bio = models.TextField(max_length=500, blank=True)

    def __str__(self):
        return self.user.username


@receiver(post_save, sender=User)
def create_or_update_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
    instance.profile.save()


class Network(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=128)
    nodes = models.IntegerField()
    edges = models.IntegerField()
    directed = models.BooleanField()
    added_at = models.DateTimeField()
    graph = JSONField()

    def __str__(self):
        return self.name

    # extract network model attributes from networkX graph and save a new model instance
    def save_nx_graph(self, graph_data, network_name, user):
        self.user = user
        self.directed = is_directed(graph_data)
        self.nodes = graph_data.number_of_nodes()
        self.edges = graph_data.number_of_edges()
        self.graph = json_graph.node_link_data(graph_data)
        self.added_at = timezone.now()
        self.name = network_name
        self.save()

    def get_nx_graph(self):
        return json_graph.node_link_graph(self.graph)


class Experiment(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    started_at = models.DateTimeField()


class Diffusion(models.Model):
    DIFFUSION_MODELS = (
        ('ltm', 'Linear Threshold Model'),
        ('icm', 'Independent Cascade Model')
    )
    network = models.ForeignKey(Network, on_delete=models.CASCADE)
    experiment = models.ForeignKey(Experiment, on_delete=models.CASCADE)
    model = models.CharField(
        max_length=128,
        choices=DIFFUSION_MODELS
    )
    model_param = models.CharField(max_length=64)
    steps = models.IntegerField()
    cover = models.FloatField()
    seed_criteria = models.CharField(max_length=128)
    seed_value = models.CharField(max_length=16)
    diffusion_steps = JSONField()


@receiver(models.signals.post_delete, sender=Diffusion)
def delete_empty_experiment(sender, instance, *args, **kwargs):
    experiment_id = instance.experiment_id
    if Experiment.objects.filter(id=experiment_id).exists() and not Diffusion.objects.filter(
            experiment_id=experiment_id).count():
        experiment_object = Experiment.objects.get(pk=experiment_id)
        experiment_object.delete()
