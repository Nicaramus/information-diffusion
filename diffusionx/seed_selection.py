import operator
from random import Random
import networkx as nx
random = Random()


# get 'nodes_number' different nodes at random
def random_nodes(network, nodes_number):
    if nodes_number >= nx.number_of_nodes(network):
        return list(network.nodes())
    else:
        return random_subset(network.nodes(), nodes_number)


def random_subset(iterator, k):
    result = []
    n = 0
    for item in iterator:
        n += 1
        if len(result) < k:
            result.append(item)
        else:
            s = int(random.random() * n)
            if s < k:
                result[s] = item
    return result


# get 'nodes_number' nodes with highest degree centrality value
def highest_degree(network, nodes_number):
    degree_list = nx.degree_centrality(network)
    sorted_degree = sorted(degree_list.items(), key=operator.itemgetter(1), reverse=True)
    return [node[0] for node in sorted_degree[:nodes_number]]


# get 'nodes_number' nodes with highest closeness centrality value
def highest_closeness(network, nodes_number):
    closeness_list = nx.closeness_centrality(network)
    sorted_closeness = sorted(closeness_list.items(), key=operator.itemgetter(1), reverse=True)
    return [node[0] for node in sorted_closeness[:nodes_number]]


# get 'nodes_number' nodes with highest betweenness centrality value
def highest_betweenness(network, nodes_number):
    betweenness_list = nx.betweenness_centrality(network)
    sorted_betweenness = sorted(betweenness_list.items(), key=operator.itemgetter(1), reverse=True)
    return [node[0] for node in sorted_betweenness[:nodes_number]]


# get 'nodes_number' nodes with highest eigenvector centrality value
def highest_eigenvector(network, nodes_number):
    eigenvector_list = nx.betweenness_centrality(network)
    sorted_eigenvector = sorted(eigenvector_list.items(), key=operator.itemgetter(1), reverse=True)
    return [node[0] for node in sorted_eigenvector[:nodes_number]]


