from django.test import TestCase
from random import Random
from unittest.mock import patch
import networkx as nx
import diffusionx.experiment as experiment
import diffusionx.seed_selection as seed_selection


class TestSeed(TestCase):
    def setUp(self):
        self.random = Random(666)

        self.graph = nx.DiGraph()
        self.graph.add_edge(1, 2)
        self.graph.add_edge(1, 3)
        self.graph.add_edge(2, 5)
        self.graph.add_edge(4, 2)
        self.graph.add_edge(5, 6)
        self.graph.add_edge(6, 3)
        self.graph.add_edge(7, 3)

    @patch('diffusionx.seed_selection.random')
    def test_random_subset(self, random):
        random.random._mock_side_effect = self.random.random
        set_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

        subset1 = seed_selection.random_subset(set_list, 6)
        self.assertListEqual(subset1, [1, 2, 3, 9, 5, 10])

        subset2 = seed_selection.random_subset(set_list, 3)
        self.assertListEqual(subset2, [7, 2, 5])

        subset3 = seed_selection.random_subset(set_list, 0)
        self.assertListEqual(subset3, [])

        subset4 = seed_selection.random_subset(set_list, 15)
        self.assertListEqual(subset4, set_list)

    def test_get_nodes_subset_happy_path(self):
        nodes_number = 3
        result_nodes = experiment.select_seed_nodes_by_number(self.graph, 'random', nodes_number)
        self.assertEqual(nodes_number, len(set(result_nodes)))
        self.assertTrue(self, set(result_nodes).issubset(set(self.graph.nodes())))

    def test_get_nodes_subset_when_number_is_zero(self):
        nodes_number = 0
        result_nodes = experiment.select_seed_nodes_by_number(self.graph, 'random', nodes_number)
        self.assertEqual(result_nodes, [])

    def test_get_nodes_subset_when_number_is_negative(self):
        nodes_number = -3
        with self.assertRaises(ValueError):
            experiment.select_seed_nodes_by_number(self.graph, 'random', nodes_number)

    @patch('diffusionx.seed_selection.random')
    def test_get_seed_by_number(self, random):
        random.random._mock_side_effect = self.random.random

        nodes, _ = experiment.select_seed(self.graph, 'random', 4, 'number')
        self.assertListEqual(nodes, [1, 2, 4, 7])

        nodes, _ = experiment.select_seed(self.graph, 'random', 15, 'number')
        self.assertListEqual(nodes, list(self.graph.nodes()))

        nodes, _ = experiment.select_seed(self.graph, 'random', 0, 'number')
        self.assertListEqual(nodes, [])

        with self.assertRaises(ValueError):
            nodes, _ = experiment.select_seed(self.graph, 'random', -1, 'number')

    @patch('diffusionx.seed_selection.random')
    def test_get_seed_by_percentage(self, random):
        random.random._mock_side_effect = self.random.random
        nodes, _ = experiment.select_seed(self.graph, 'random', 1, 'percentage')
        self.assertListEqual(nodes, [])

        nodes, _ = experiment.select_seed(self.graph, 'random', 20, 'percentage')
        self.assertListEqual(nodes, [2])

        nodes, _ = experiment.select_seed(self.graph, 'random', 50, 'percentage')
        self.assertListEqual(nodes, [7, 2, 3, 5])

        nodes, _ = experiment.select_seed(self.graph, 'random', 100, 'percentage')
        self.assertListEqual(nodes, list(self.graph.nodes()))

        nodes, _ = experiment.select_seed(self.graph, 'random', 0.001, 'percentage')
        self.assertListEqual(nodes, [])

        with self.assertRaises(ValueError):
            experiment.select_seed(self.graph, 'random', -50, 'percentage')

        with self.assertRaises(ValueError):
            experiment.select_seed(self.graph, 'random', 150, 'percentage')
