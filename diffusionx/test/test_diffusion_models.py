from django.test import TestCase
import networkx
from diffusionx.diffusion_models import independent_cascade, linear_threshold


class TestIndependentCascade(TestCase):
    def setUp(self):
        self.graph = networkx.DiGraph()
        self.graph.add_edge(1, 2)
        self.graph.add_edge(1, 3)
        self.graph.add_edge(2, 5)
        self.graph.add_edge(4, 2)
        self.graph.add_edge(5, 6)
        self.graph.add_edge(6, 3)
        self.graph.add_edge(7, 3)

    def test_spread_for_static_prob_values(self):
        self.assertEqual(independent_cascade.spread(self.graph, [1], common_prob=1), [[1], [2, 3], [5], [6]])

        networkx.set_edge_attributes(self.graph, 0.0, 'prob')
        self.assertEqual(independent_cascade.spread(self.graph, [1], common_prob=0), [[1]])

    def test_spread_for_random_prob(self):
        self.assertEqual(independent_cascade.spread(self.graph, [1], seed_number=57, common_prob=0.4), [[1], [2], [5]])

        self.assertEqual(independent_cascade.spread(self.graph, [1], seed_number=40, common_prob=0.4), [[1]])

    def test_invalid_seed(self):
        with self.assertRaises(Exception):
            independent_cascade.spread(self.graph, [9])


class TestLinearThreshold(TestCase):
    def setUp(self):
        self.graph = networkx.DiGraph()
        self.graph.add_edge(1, 2)
        self.graph.add_edge(1, 3)
        self.graph.add_edge(2, 5)
        self.graph.add_edge(4, 2)
        self.graph.add_edge(5, 6)
        self.graph.add_edge(6, 3)
        self.graph.add_edge(7, 3)

    def test_spread_for_static_prob_values(self):
        self.assertEqual(linear_threshold.spread(self.graph, [1], common_threshold=0), [[1], [2, 3], [5], [6]])

        networkx.set_edge_attributes(self.graph, 0.0, 'prob')
        self.assertEqual(linear_threshold.spread(self.graph, [1], common_threshold=1), [[1]])

    def test_invalid_seed(self):
        with self.assertRaises(Exception):
            linear_threshold.spread(self.graph, [9])
