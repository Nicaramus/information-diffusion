from django.test import TestCase
from django.contrib.auth import get_user_model
from .. import models


class TestProfileModel(TestCase):
    def test_profile_creation(self):
        user_model = get_user_model()
        user = user_model.objects.create(
            username="test-user", password="test-password")
        # Check that a Profile instance has been created
        self.assertIsInstance(user.profile, models.Profile)
        # Call the save method of the user to activate the signal again, and check
        # that it doesn't try to create another profile instance
        user.save()
        self.assertIsInstance(user.profile, models.Profile)
