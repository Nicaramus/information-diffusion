var allowTabInput = function (inputEl) {
    inputEl.keydown(function (e) {
        var TABKEY = 9;
        if (e.keyCode == TABKEY) {
            this.value += "    ";
            if (e.preventDefault) {
                e.preventDefault();
            }
            return false;
        }
    });
};

// Drag and drop box
var initDragAndDropBox = function () {
    var mouseOverClass = "mouse-over";

    var dropZone = $("#drop-zone");
    var inputFile = dropZone.find("input");
    dropZone.on("dragover", function (event) {
        event.preventDefault();
        event.stopPropagation();

        var oLeft = dropZone.offset().left;
        var oRight = dropZone.outerWidth() + oLeft;
        var oTop = dropZone.offset().top;
        var oBottom = dropZone.outerHeight() + oTop;

        var x = event.pageX;
        var y = event.pageY;
        dropZone.addClass(mouseOverClass);

        if (!(x < oLeft || x > oRight || y < oTop || y > oBottom)) {
            inputFile.offset({top: y - 15, left: x - 15});
        } else {
            inputFile.offset({top: -400, left: -400});
        }
    });

    var clickZone = $("#clickHere");

    clickZone.mousemove(function (e) {
        var left = clickZone.offset().left;
        var right = clickZone.outerWidth() + left;
        var top = clickZone.offset().top;
        var bottom = clickZone.outerHeight() + top;
        var x = e.pageX;
        var y = e.pageY;

        if (!(x < left || x > right || y < top || y > bottom)) {
            inputFile.offset({top: y - 15, left: x - 15});
        } else {
            inputFile.offset({top: -400, left: -400});
        }
    });


    dropZone.on("dragleave", function () {
        event.preventDefault();
        event.stopPropagation();
        dropZone.removeClass(mouseOverClass);
    });
    dropZone.on("drop", function () {
        dropZone.removeClass(mouseOverClass);
    });
};


$(document).ready(function () {
    initDragAndDropBox();

    allowTabInput($('input[name="delimiter"]'));

    // event after action of selecting file
    $('#file').on("change", function () {
        var fileInput = document.getElementById("file");
        var fileIcon = $(".fa-file-alt");
        $('#drop-zone').css("border-color", "");
        if (fileInput.files.length > 0) {
            // write filename of the selected file upon the "drag and drop" box
            var fileName = $('#filename');
            fileName.text(fileInput.files[0].name);
            fileName.css('color', '');
            $('#drop-file').text('');
            fileIcon.animate({opacity: '1'}, "slow");
        } else {
            $('#filename').text("No file chosen");
            $('#drop-file').text('Drop file here...');
            fileIcon.animate({opacity: '0'}, "slow");
        }
    });

    // disable other forms than selected one in the tab where a network is chosen
    $("#network-select").change(function () {
        $(".network-option").hide();
        $(".network-option :input").prop('disabled', true);

        var selected = $("#network-" + $("#network-select").val());
        selected.show();
        selected.find(":input").prop('disabled', false);
    });

    // network graph generators forms
    var graphSelect = $('#random-graph');
    // get a form of a network graph generator
    graphSelect.on("change", function () {
        var id = $('#random-graph').val();
        $.ajax({
            type: 'GET',
            url: '/graph/' + id + '/',
            success: function (data) {
                $('#graph-parameters').html(data);
                $('input[data-toggle="tooltip"]').tooltip();
            }
        });
    });

    // initialize network form
    graphSelect.trigger("change");

    // jquery validation plugin - add methods
    $.validator.addMethod("lessThan",
        function (value, element, param) {
            var $greaterValue = $(param);
            if (this.settings.onfocusout) {
                $greaterValue.off(".validate-lessThan").on("blur.validate-lessThan", function () {
                    $(element).valid();
                });
            }
            return parseInt(value) < parseInt($greaterValue.val());
        }, "The value must be less than the other one"
    );
    $.validator.addMethod("greaterThan",
        function (value, element, param) {
            var $secondValue = $(param);
            if (this.settings.onfocusout) {
                $secondValue.off(".validate-greaterThan").on("blur.validate-greaterThan", function () {
                    $(element).valid();
                });
            }
            return parseInt(value) > parseInt($secondValue.val());
        }, "The value must be greater than the other one"
    );
    $.validator.addMethod("evenProduct",
        function (value, element, param) {
            var $multiplier = $(param);
            if (this.settings.onfocusout) {
                $multiplier.off(".validate-evenProduct").on("blur.validate-evenProduct", function () {
                    $(element).valid();
                });
            }
            return (parseInt(value) * parseInt($multiplier.val())) % 2 == 0;
        }, "The value of N×D must be even."
    );

    var form = $("#network_form");

    form[0].onsubmit = function () {
        loadingOn();
    };

    form.validate({
        rules: {
            network_name: {
                required: true,
                minlength: 3,
                maxlength: 128
            },
            file: {
                required: true,
                extension: "mtx|edges|edgelist|csv|tsv"
            },
            konect_input: {
                url: true
            },
            bag_m: {
                lessThan: '#bag_n'
            },
            k: {
                lessThan: '#id_n'
            },
            rg_n: {
                greaterThan: '#id_rg_d',
                evenProduct: '#id_rg_d'
            }
        },
        messages: {
            network_name: {
                required: "Name your graph to identify it easier",
                maxlength: "Graph name cannot be larger than 128 characters"
            },
            file: {
                required: "The graph file is required"
            },
            konect_input: {
                url: true
            },
            bag_m: {
                lessThan: "M should be less than N"
            },
            k: {
                lessThan: 'Choose smaller K or larger N'
            },
            rg_n: {
                greaterThan: 'N should be greater than D',
                evenProduct: 'The value of N×D must be even.'
            }
        },
        invalidHandler: function () {
            // display validation error in case of the file input
            if ($('#network-select').val() == 1) {
                if ($("#file-error").css('display') != 'none') {
                    $('#drop-zone').css("border-color", "#d4061a");
                    var fileName = $('#filename');
                    fileName.text($("#file-error").text());
                    fileName.css('color', '#d4061a');
                }
            }
            loadingOff();
        }
    }).settings.ignore = ":disabled,:hidden";
});