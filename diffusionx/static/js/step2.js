//required isInt function from function.js file

let seedCriteria = {};
$("#select_seed_criteria").children().each(function () {
    seedCriteria[this.value] = this.innerHTML
});

Step2 = {
    notReadyMsg: "Choose any method of selecting initially activated nodes",

    get: function () {
        window.location.href = "/experiment/step/2";
    },

    init: function () {
        $('input[name="radio_seed"]').change(function () {
            Step2.toggleSeedRadios()
        });
        let $radios = $('input:radio[name=radio_seed]');
        if ($radios.is(':checked') === false) {
            $radios.filter('[value=percent]').click();
        }
        Step2.loadState();
    },

    saveState: function () {
        return $.ajax({
            url: '../state/2',
            contentType: 'application/json; charset=utf-8',
            type: "POST",
            dataType: 'text',
            data: Step2.getJsonResults()
        });
    },

    isReady: function () {
        return !Step2.isSeedCriteriaEmpty();
    },

    loadState: function () {
        $.getJSON("../state/2", function (data) {
            let items = [];
            $.each(data, function (key, val) {
                items.push(val);
            });

            items.reverse().forEach(function (seedItem) {
                Step2.addSeedCriteria(seedItem.value, seedItem.value_type, seedItem.seed_criteria);
            });
        });
    },

    isSeedCriteriaEmpty: function () {
        return !$('#selected_table').find('tr').length;
    },

    getJsonResults: function () {
        let data = [];
        $('#selected_table').find('tr').each(function () {
            let seedDescriptionCell = $(this).find('.criteria_description');
            let seedCriteriaID = seedDescriptionCell.data('seed_criteria');
            let nodeValue = $(this).find('.criteria_value');
            let value = nodeValue.data('value');
            let valueType = nodeValue.data('value_type');
            data.push({'value': value, 'value_type': valueType, 'seed_criteria': seedCriteriaID});
        });
        return JSON.stringify(data);
    },

    hideNodesNumber: function () {
        let elem = $("#nodes_number_container");
        elem.css('display', 'none');
        elem.find("input").prop('disabled', true);
    },

    showNodesNumber: function () {
        let elem = $("#nodes_number_container");
        elem.css('display', 'inline-block');
        elem.find("input").prop('disabled', false);
    },

    hideNodesPercent: function () {
        let elem = $("#nodes_percent_container");
        elem.css('display', 'none');
        elem.find("input").prop('disabled', true);
    },

    showNodesPercent: function () {
        let elem = $("#nodes_percent_container");
        elem.css('display', 'inline-block');
        elem.find("input").prop('disabled', false);
    },

    toggleSeedRadios: function () {
        let checked = $('input[name="radio_seed"]:checked');
        if (checked.val() === 'number') {
            Step2.showNodesNumber();
            Step2.hideNodesPercent();
        } else if (checked.val() === 'percent') {
            Step2.showNodesPercent();
            Step2.hideNodesNumber();
        }
    },

    addSeedCriteria: function (value, valueType, seedCriteriaID) {
        //get table row template
        let template = document.querySelector('#selected_criteria_row');
        let documentFragment = template.content;
        let templateClone = documentFragment.cloneNode(true);

        //set row data - visible text and hidden data in case of removing the element from the seed array
        let seedDescriptionCell = $(templateClone).find('.criteria_description');
        seedDescriptionCell.data('seed_criteria', seedCriteriaID);
        seedDescriptionCell.text(seedCriteria[seedCriteriaID]);
        let nodeValue = $(templateClone).find('.criteria_value');
        nodeValue.data('value', value);
        nodeValue.data('value_type', valueType);
        if (valueType == 'percentage') {
            value = value + "%";
        }
        nodeValue.text(value);

        //hide empty list message
        Step2.hideEmptySeedMessage();
        //add the row to the top of the table
        $('#selected_table').prepend(templateClone);
    },

    addSeedNodesPercentage: function () {
        let percentage = $('input[name="nodes_percentage"]').val();

        let seedId = $('#select_seed_criteria').val();

        if (percentage == null || percentage == "") {
            //TODO show info message
        } else {
            if (isNumber(percentage) && percentage >= 1 && percentage <= 100)
                Step2.addSeedCriteria(percentage, 'percentage', seedId);
        }
    },

    addSeedNodesNumber: function () {
        let number = $('input[name="nodes_number"]').val();

        let seedId = $('#select_seed_criteria').val();

        if (number == null || number == "") {
            //TODO show info message
        } else {
            if (number > 0 && isInt(number))
                Step2.addSeedCriteria(number, 'number', seedId);
            //todo add else situation when info messages is shown
        }
    },

    removeCriteria: function (removeButton) {
        let criteriaRow = $(removeButton).parent('td').parent('tr');
        criteriaRow.remove();
        if (Step2.isSeedCriteriaEmpty()) {
            Step2.showEmptySeedMessage();
        }
    },

    hideEmptySeedMessage: function () {
        $('#empty_list').hide();
        removeErrorMessage();
    },

    showEmptySeedMessage: function () {
        $('#empty_list').show();
    }
};