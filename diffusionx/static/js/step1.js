Step1 = {
    notReadyMsg: "At least one network must be selected",

    get: function() {
        window.location.href = "/experiment/step/1";
    },

    init: function () {
        $("#network-select-list").find(".add-network").each(function () {
            Step1.addAddButtonListener(this);
        });
        $("#network-chosen-list").find(".btn").each(function () {
            Step1.addRemoveButtonListener(this);
        });
        Step1.updateSelectList();
        Step1.updateChosenList();
    },

    saveState: function () {
        return $.ajax({
            url: '../state/1',
            type: "POST",
            data: {
                networks: Step1.getChosenNetworksIds()
            }
        });
    },

    isReady: function () {
        return !Step1.isChosenListEmpty();
    },

    isSelectListEmpty: function () {
        return !$("#network-select-list").find("tr.network-row").length;
    },

    isChosenListEmpty: function () {
        return !$("#network-chosen-list").find("tr.network-row").length;
    },

    updateChosenList: function () {
        let emptyListInfo = $('#empty-chosen-list');
        if (Step1.isChosenListEmpty()) {
            emptyListInfo.css('display', 'flex');
        } else {
            emptyListInfo.hide();
        }
    },

    updateSelectList: function () {
        let emptyListInfo = $('#empty-select-list');
        if (Step1.isSelectListEmpty() && Step1.isChosenListEmpty()) {
            emptyListInfo.css('display', 'flex');
        } else {
            emptyListInfo.hide();
        }
    },

    getChosenNetworksIds: function () {
        let data = $('#network-chosen-list').find('.btnCell .btn').map(function () {
            return $(this).data('id');
        }).get();
        return data;
    },

    addRemoveButtonListener: function (button) {
        $(button).click(function () {
            //remove network from the list of networks and add it to the list of selected networks
            let id = $(this).data("id");
            let network = $('#network-' + id);
            $("#network-select-list").append(network);
            network.find(".btn").remove();
            let btnCell = network.find('.btnCell');
            btnCell.append('<button class="btn btn-add add-network">Add</button>');
            let button = btnCell.find(".btn");
            button.data('id', id);
            Step1.addAddButtonListener(button);

            Step1.updateSelectList();
            Step1.updateChosenList();
        })
    },

    addAddButtonListener: function (button) {
        $(button).click(function () {
            let id = $(this).data("id");
            let network = $('#network-' + id);
            network.find(".btn").remove();
            $("#network-chosen-list").append(network);
            let btnCell = network.find('.btnCell');
            btnCell.append('<button class="btn btn-danger">Remove</button>');
            let button = btnCell.find(".btn");
            button.data('id', id);
            Step1.addRemoveButtonListener(button);

            Step1.updateSelectList();
            Step1.updateChosenList();

            removeErrorMessage();
        });
    },


    filterByName: function () {
        //dont filter if there is no network at all
        if (Step1.isSelectListEmpty()) {
            return;
        }

        let input = $("#searchInput");
        let filter = input.val().toUpperCase();

        let counter = 0;
        $("#network-select-list").find("tr.network-row").each(function () {
            var td = $(this).find("td").first();
            if (td) {
                if (td.text().toUpperCase().indexOf(filter) > -1) {
                    counter++;
                    $(this).css("display", "");
                } else {
                    $(this).css("display", "none");
                }
            }
        });

        if (counter > 0) {
            $('#filter-info').hide();
            $('#network-select-list').show();
        } else {
            $('#network-select-list').hide();
            $('#filter-info').css('display', 'flex');
        }
    },
};
