function loadingOn() {
    $('.loading').fadeIn("slow", function () {
        $(this).css('display', 'block');
    });
}

function loadingOff() {
    $('.loading').fadeOut("slow", function () {
        $(this).css('display', 'none');
    });
}

function isInt(value) {
    return !isNaN(value) &&
        parseInt(Number(value)) == value &&
        !isNaN(parseInt(value, 10));
}

function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

function isFraction(n) {
    return isNumber(n) && n >= 0 && n <= 1;
}

function initConfirmDialog() {
    $('#confirm-dialog').on('show.bs.modal', function (e) {
        $message = $(e.relatedTarget).attr('data-message');
        $(this).find('.modal-body p').text($message);
        $title = $(e.relatedTarget).attr('data-title');
        $(this).find('.modal-title').text($title);

        let form = $(e.relatedTarget).closest('form');
        $(this).find('.modal-footer #confirm').data('form', form);
    });

    $('#confirm-dialog').find('.modal-footer #confirm').on('click', function () {
        $(this).data('form').submit();
    });
}
