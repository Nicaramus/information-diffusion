var maxGraphSize = 700;

var drawGraph = function (currentGraphSize, networkId) {
    var svg = d3.select("svg"),
        width = +svg.attr("width"),
        height = +svg.attr("height");

    var color = d3.scaleOrdinal(d3.schemeCategory20);

    var simulation = d3.forceSimulation()
        .force("link", d3.forceLink().id(function (d) {
            return d.id;
        }))
        .force("charge", d3.forceManyBody())
        .force("center", d3.forceCenter(width / 2, height / 2));

    if (currentGraphSize <= maxGraphSize) {
        d3.json("/get_json_graph/" + networkId, function (error, graph) {
                if (error) throw error;

                var link = svg.append("g")
                    .attr("class", "links")
                    .selectAll("line")
                    .data(graph.links)
                    .enter().append("line")
                    .attr("stroke-width", function (d) {
                        return Math.sqrt(d.value);
                    });

                var node = svg.append("g")
                    .attr("class", "nodes")
                    .selectAll("circle")
                    .data(graph.nodes)
                    .enter().append("circle")
                    .attr("r", 7)
                    .call(d3.drag()
                        .on("start", dragstarted)
                        .on("drag", dragged)
                        .on("end", dragended));

                node.append("title")
                    .text(function (d) {
                        return d.id;
                    });

                node.attr('id', function (d) {
                    return 'id' + d.id;
                });

                simulation
                    .nodes(graph.nodes)
                    .on("tick", ticked);

                simulation.force("link")
                    .links(graph.links);

                function ticked() {
                    link
                        .attr("x1", function (d) {
                            return d.source.x;
                        })
                        .attr("y1", function (d) {
                            return d.source.y;
                        })
                        .attr("x2", function (d) {
                            return d.target.x;
                        })
                        .attr("y2", function (d) {
                            return d.target.y;
                        });

                    node
                        .attr("cx", function (d) {
                            return d.x;
                        })
                        .attr("cy", function (d) {
                            return d.y;
                        });
                }
            }
        );
    } else {
        var svg = d3.select("svg");
        svg.append('text').attr("x", "50%").attr("y", "50%").attr('alignment-baseline', 'middle')
            .attr('text-anchor', 'middle').attr('font-size', '20pt')
            .text(`More than ${maxGraphSize}\u0020nodes`);
    }


    function dragstarted(d) {
        if (!d3.event.active) simulation.alphaTarget(0.3).restart();
        d.fx = d.x;
        d.fy = d.y;
    }

    function dragged(d) {
        d.fx = d3.event.x;
        d.fy = d3.event.y;
    }

    function dragended(d) {
        if (!d3.event.active) simulation.alphaTarget(0);
        d.fx = null;
        d.fy = null;
    }
};


var diffusionId;
var stepTime;
var changeColorDuration;
var diffusions;
var time = ms => new Promise(res => setTimeout(res, ms));


function getDiffusions() {
    loadingOn();
    d3.json("/get_json_diffusion/" + diffusionId, function (data) {
        this.diffusions = JSON.parse(data);
        initSeed();
        loadingOff();
    });
}

function animationStep(nodesArray, duration) {
    nodesArray.forEach(function (node) {
        d3.select('#id' + node).transition()
            .duration(duration)
            .style('fill', 'red');
    });
}

function setStepStatus(stepNumber, activeNodes) {
    d3.select(".diffusion-step-status .status-value").text(stepNumber);
    d3.select(".active-nodes-status .status-value").text(activeNodes);
    var cover = activeNodes/currentGraphSize * 100;
    d3.select(".cover-status .status-value").text(parseFloat(cover.toFixed(4)) + '%');
}

function initSeed() {
    d3.selectAll('.nodes circle').style('fill', '#84dbff');
    this.animationStep(diffusions[0], 0);
    setStepStatus(1, diffusions[0].length);
    return diffusions[0].length;
}

async function runAnimation() {
    var startBtn =  $('#animation-start');
    startBtn.attr("disabled", true);
    startBtn.text("Running...");
    var activeNodes = this.initSeed();
    await this.time(this.stepTime);
    for (let i = 1; i < diffusions.length; i++) {
        this.animationStep(diffusions[i], changeColorDuration);
        activeNodes += diffusions[i].length;
        setStepStatus(i+1, activeNodes);
        await this.time(this.stepTime);
    }
    startBtn.attr("disabled", false);
    startBtn.text("Restart");
}




