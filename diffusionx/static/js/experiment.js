"use strict";

function getCookie(cName) {
    var cStart;
    var cEnd;
    if (document.cookie.length > 0) {
        cStart = document.cookie.indexOf(cName + "=");
        if (cStart != -1) {
            cStart = cStart + cName.length + 1;
            cEnd = document.cookie.indexOf(";", cStart);
            if (cEnd == -1) cEnd = document.cookie.length;
            return unescape(document.cookie.substring(cStart, cEnd));
        }
    }
    return "";
}

function showErrorMessage(errorMsg) {
    let errorAlert = `
        <div class="alert alert-danger alert-dismissible">
            ${errorMsg}
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        </div>`;

    removeErrorMessage();

    $('.container').prepend(errorAlert);
}

function removeErrorMessage() {
    let lastAlert = $('.container').find('.alert.alert-danger');

    if (lastAlert.length) {
        lastAlert.remove();
    }
}

function setCSRF() {
    $.ajaxSetup({
        headers: {"X-CSRFToken": getCookie("csrftoken")}
    });
}

function setNextBtn() {
    let stepsNumber = $('#nav-field').find('.tab-experiment a').length;
    let nextBtn = $('#nextStep');

    if (step !== stepsNumber) { //if not the last step
        nextBtn.html('Next');
        nextBtn.click(() => {
            let currentStep = getStepNamespace(step);
            if (currentStep.isReady()) {
                goStep(step + 1);
            } else {
                showErrorMessage(currentStep.notReadyMsg);
            }
        });
    } else {
        nextBtn.html('Submit');
        nextBtn.click(() => {
            let currentStep = getStepNamespace(step);
            if (currentStep.isReady()) {
                currentStep.saveState().done(function (data) {
                    loadingOn();
                    if (data === "Success") {
                            $('#run-experiment-form').submit();
                    }
                    loadingOff();
                });
            } else {
                showErrorMessage(currentStep.notReadyMsg);
            }
        })
    }
}

function setPreviousBtn() {
    if (step !== 1) { //if not the first step
        let previousBtn = $('#previousStep');
        previousBtn.removeClass('disabled');
        previousBtn.removeClass('btn-secondary');
        previousBtn.addClass('btn-primary');
        previousBtn.click(() => {
            goStep(step - 1);
        });
    }
}

function setNavigationButtons(currentStep) {
    setPreviousBtn();
    setNextBtn();

    $('#nav-field').find('.tab-experiment a').each(function (index, link) {
        if (index == currentStep - 1) {
            $(link).removeClass().addClass('selected');
        }
        // else {
        //     $(link).removeClass().addClass('active');
        // }
    });
}

function init(step) {
    let stepPage = getStepNamespace(step);
    stepPage.init();
    setNavigationButtons(step);
}

function goStep(stepNumber) {
    if (step != stepNumber) {
        let currentStep = getStepNamespace(step);
        currentStep.saveState().done(function (data) {
            if (data === "Success") {
                let targetStep = getStepNamespace(stepNumber);
                targetStep.get();
            }
        });
    }
}

function getStepNamespace(stepNumber) {
    switch (stepNumber) {
        case 1:
            var stepObj = Step1;
            break;
        case 2:
            var stepObj = Step2;
            break;
        case 3:
            var stepObj = Step3;
    }

    return stepObj;
}

(function () {
    setCSRF();
    init(step);
}(jQuery));


