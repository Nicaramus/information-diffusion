Step3 = {
    notReadyMsg: "Choose any diffusion model and its parameters",

    diffusionModels: {
        ltm: 'Linear Threshold Model',
        icm: 'Independent Cascade Model'
    },

    valueDescription: {
        ltm: 'Threshold value',
        icm: 'Activation probability'
    },

    get: function () {
        window.location.href = "/experiment/step/3";
    },

    init: function () {
        $('input[name="icm_parameter"]').change(function () {
            Step3.radioIcmMenu();
        });
        $('input[name="ltm_parameter"]').change(function () {
            Step3.radioLtmMenu();
        });
        $('#icm_properties_button').click(function () {
            Step3.clickIcmMenu();
        });
        $('#ltm_properties_button').click(function () {
            Step3.clickLtmMenu();
        });
        Step3.loadState();
    },

    isReady: function () {
        return true;
    },

    saveState: function () {
        return $.ajax({
            url: '../state/3',
            contentType: 'application/json; charset=utf-8',
            type: "POST",
            dataType: 'text',
            data: Step3.getJsonResults(),
            success: function (data) {
                console.log(data);
            }
        });
    },

    loadState: function () {
        $.getJSON("../state/3", function (data) {
            let items = [];
            $.each(data, function (key, val) {
                items.push(val);
            });

            items.reverse().forEach(function (item) {
                if (item.option == 'single') {
                    Step3.addSingleCriteria(item.value,  Step3.valueDescription[item.model], item.model);
                } else if (item.option == 'series') {
                    Step3.addSeriesCriteria(item.value.from, item.value.to, item.value.step, Step3.valueDescription[item.model], item.model);
                }
            });
        });
    },

    getJsonResults: function () {
        let data = [];
        $('#selected_table').find('tr').each(function () {
            if ($(this).hasClass('single')) {
                let option = "single"
                let modelEl = $(this).find('.criteria_description')
                let model = modelEl.data('model');
                let valueNode = $(this).find('.criteria_value');
                let value = valueNode.data('value');
                data.push({'model': model, 'option': option, 'value': value});
            } else if ($(this).hasClass('series')) {
                let option = "series";
                let modelEl = $(this).find('.criteria_description')
                let model = modelEl.data('model');
                let fromEl = $(this).find('.criteria_value.from span');
                let from = fromEl.data('value');
                let toEl = $(this).find('.criteria_value.to span');
                let to = toEl.data('value');
                let stepEl = $(this).find('.criteria_value.step span');
                let step = stepEl.data('value');
                data.push({
                    'model': model, 'option': option, 'value': {
                        'from': from,
                        'to': to,
                        'step': step
                    }
                });
            }
        });
        return JSON.stringify(data);
    },


    isReady: function () {
        return !Step3.isSelectionTableEmpty();
    },

    isSelectionTableEmpty: function () {
        return !$('#selected_table').find('tr').length;
    },

    radioIcmMenu: function () {
        let checked = $('input[name="icm_parameter"]:checked');
        if (checked.val() === 'single') {
            Step3.radioIcmSingle();
        } else if (checked.val() === 'series') {
            Step3.radioIcmSeries();
        }
    },

    radioLtmMenu: function () {
        let checked = $('input[name="ltm_parameter"]:checked');
        if (checked.val() === 'single') {
            Step3.radioLtmSingle();
        } else if (checked.val() === 'series') {
            Step3.radioLtmSeries();
        }
    },

    radioIcmSingle: function () {
        let menuSingle = $("#icm_single_container");
        let menuSeries = $("#icm_series_container");

        menuSeries.css('display', 'none');
        menuSingle.css('display', 'inline-block');
    },

    radioIcmSeries: function () {
        let menuSingle = $("#icm_single_container");
        let menuSeries = $("#icm_series_container");

        menuSingle.css('display', 'none');
        menuSeries.css('display', 'inline-block');
    },

    radioLtmSingle: function () {
        let menuSingle = $("#ltm_single_container");
        let menuSeries = $("#ltm_series_container");

        menuSeries.css('display', 'none');
        menuSingle.css('display', 'inline-block');
    },

    radioLtmSeries: function () {
        let menuSingle = $("#ltm_single_container");
        let menuSeries = $("#ltm_series_container");

        menuSingle.css('display', 'none');
        menuSeries.css('display', 'inline-block');
    },

    clickIcmMenu: function () {
        $('#icm_properties_container').css('display', 'block');
        $('#ltm_properties_container').css('display', 'none');
        Step3.radioIcmMenu();
    },

    clickLtmMenu: function () {
        $('#ltm_properties_container').css('display', 'block');
        $('#icm_properties_container').css('display', 'none');
        Step3.radioLtmMenu();
    },

    addSingleCriteria: function (value, valueDescription, model) {
        //get table row template
        let template = document.querySelector('#selected_criteria_row');
        let documentFragment = template.content;
        let templateClone = documentFragment.cloneNode(true);

        //set row data - visible text and hidden data in case of removing the element from the seed array
        let modelType = $(templateClone).find('.criteria_description');
        modelType.data('model', model);
        modelType.text(Step3.diffusionModels[model]);

        let valueType = $(templateClone).find('.criteria_type');
        valueType.data('type', valueDescription);
        valueType.text(valueDescription);

        let conditionValue = $(templateClone).find('.criteria_value');
        conditionValue.data('value', value);
        conditionValue.text(value);

        //hide empty list message
        Step3.hideEmptySeedMessage();
        //add the row to the top of the table
        $('#selected_table').prepend(templateClone);
    },

    addSeriesCriteria: function (from, to, step, valueDescription, model) {
        //get table row template
        let template = document.querySelector('#selected_series_criteria_row');
        let documentFragment = template.content;
        let templateClone = documentFragment.cloneNode(true);

        //set row data - visible text and hidden data in case of removing the element from the seed array
        let modelType = $(templateClone).find('.criteria_description');
        modelType.data('model', model);
        modelType.text(Step3.diffusionModels[model]);

        let valueType = $(templateClone).find('.criteria_type');
        valueType.data('type', valueDescription);
        valueType.text(valueDescription);

        let fromValue = $(templateClone).find('.criteria_value.from span');
        fromValue.data('value', from);
        fromValue.text(from);

        let toValue = $(templateClone).find('.criteria_value.to span');
        toValue.data('value', to);
        toValue.text(to);

        let stepValue = $(templateClone).find('.criteria_value.step span');
        stepValue.data('value', step);
        stepValue.text(step);

        //hide empty list message
        Step3.hideEmptySeedMessage();
        //add the row to the top of the table
        $('#selected_table').prepend(templateClone);
    },

    removeSelectedElement: function (removeButton) {
        let criteriaRow = $(removeButton).parent('td').parent('tr');
        criteriaRow.remove();
        if (Step3.isSelectionTableEmpty()) {
            Step3.showEmptySeedMessage();
        }
    },

    addIcmSingle: function () {
        let probability = $('input[name="icm_single"]').val();
        if (isFraction(probability)) {
            Step3.addSingleCriteria(probability, Step3.valueDescription['icm'], 'icm');
        }
    },

    addIcmSeries: function () {
        let from = $('input[name="icm_from"]').val();
        let to = $('input[name="icm_to"]').val();
        let step = $('input[name="icm_step"]').val();
        if (!isNumber(from) || !isNumber(to) || !isNumber(step)) {
            //TODO show info message
        } else {
        }
        if (isFraction(from) && isFraction(to) && isInt(step)  && from < to && step > 0)
            Step3.addSeriesCriteria(from, to, step, Step3.valueDescription['icm'], 'icm');
    },

    addLtmSingle: function () {
        let threshold = $('input[name="ltm_single"]').val();
        if (isFraction(threshold)) {
            //TODO show info message
            Step3.addSingleCriteria(threshold, Step3.valueDescription['ltm'], 'ltm');
        }
    },

    addLtmSeries: function () {
        let from = $('input[name="ltm_from"]').val();
        let to = $('input[name="ltm_to"]').val();
        let step = $('input[name="ltm_step"]').val();
        if (!isNumber(from) || !isNumber(to) || !isNumber(step)) {
            //TODO show info message
        } else {
        }
        if (isFraction(from) && isFraction(to) && isInt(step) && from < to && step > 0)
            Step3.addSeriesCriteria(from, to, step, Step3.valueDescription['ltm'], 'ltm');
    },

    hideEmptySeedMessage: function () {
        $('#empty_list').hide();
        removeErrorMessage();
    },

    showEmptySeedMessage: function () {
        $('#empty_list').show();
    }
}