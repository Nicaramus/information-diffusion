# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-05-30 23:14
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('diffusionx', '0003_auto_20180516_0424'),
    ]

    operations = [
        migrations.AlterField(
            model_name='diffusion',
            name='seed_criteria',
            field=models.CharField(max_length=128),
        ),
    ]
