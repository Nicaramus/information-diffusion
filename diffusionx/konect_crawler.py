import os
from bs4 import BeautifulSoup
import requests
import re
import tarfile
import networkx as nx
import shutil


def konect_network(url):
    """ Read network from Konect network repository

        Creating NetworkX graph using a network from online repository located at http://konect.uni-koblenz.de/networks/
        Select a network from the repository by passing a url address of a network page.

        Parameters
        ----------
        url: string
            url address of a selected network from http://konect.uni-koblenz.de/networks/

        Returns
        -------
        graph: networkx.DiGraph
            NetworkX graph read from a given edgelist

        Raises
        ------
        ValueError
            passed url does not belong to http://konect.uni-koblenz.de/networks/ repository
        Exception
            - given graph is a multigraph (with multiple edges between two nodes) - not supported
            - there is no TSV network file at the given address

    """
    domain = "http://konect.uni-koblenz.de"

    # ensure that the passed url is a link to konect repository
    if domain not in url:
        raise ValueError("Wrong resource address. Visit http://konect.uni-koblenz.de/networks/ to find appropiate "
                         "network file.")
    network_name = url.rsplit('/', 1)[-1]
    page = requests.get(url)
    contents = page.content

    soup = BeautifulSoup(contents, 'html.parser')

    # only single edge is allowed between two nodes, ensure that graph is not a multigraph
    edges_type = soup.find_all('a', attrs={'href': re.compile('../help/edge_weights')})
    if re.match('^Multiple.*', edges_type[2].get_text()):
        raise Exception("Only single edge is allowed between two nodes (multigraphs not supported).")

    # look for a network TSV file
    results = soup.find_all("a", attrs={'title': re.compile('Download TSV file')})
    if not results:
        raise Exception("TSV file not found in the page.")

    # download and unpack tar file
    url = domain + results[0].get('href')[2:]
    r = requests.get(url, stream=True)
    tarfile_name = network_name + ".tar.bz2"
    with open(tarfile_name, 'wb') as f:
        for chunk in r.iter_content(chunk_size=1024):
            if chunk:
                f.write(chunk)

    tar = tarfile.open(tarfile_name, "r:bz2")
    tar.extractall(path="graph_files")
    tar.close()

    # remove tar file
    os.remove(tarfile_name)

    # find edgelist file
    for filename in os.listdir('graph_files/' + network_name):
        if re.match('^out.*', filename):
            edge_list = filename

    # determine graph format (undirected/directed) and create graph
    graph_format = soup.find_all('a', attrs={'href': re.compile('../help/network_format')})[2].get_text()
    if re.match('Directed', graph_format):
        graph = nx.read_edgelist('graph_files/' + network_name + '/' + edge_list, comments='%',
                                 data=(('weight', float),), create_using=nx.DiGraph())
    else:
        graph = nx.read_edgelist('graph_files/' + network_name + '/' + edge_list, comments='%',
                                 data=(('weight', float),), create_using=nx.Graph())

    shutil.rmtree('graph_files/' + network_name)

    return graph
