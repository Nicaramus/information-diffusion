from random import Random
import networkx as nx


def spread(graph, seed_nodes, common_threshold=None, threshold_attr='threshold', seed_number=None):
    """Propagation diffusion in linear threshold model

        Simulate diffusion process in graph structure using linear threshold model
        starting from selected nodes (seed).

        Parameters
        ----------
        graph : networkx.Graph / networkx.DiGraph
            Network structure in NetworkX graph format
        seed_nodes : list of nodes
            Seed nodes, nodes initially active
        impact_attr : string
            name of (u, v) edge attribute describing influence value of 'u' node on 'v' node
            by default it is set to (1/in_degree) where in_degree is number of edges pointing to the same node
        common_threshold : float
            threshold value set for all nodes except if None (default) - then use node attribute value or set random
            value if it does not have its own
        threshold_attr: string
            name of node attribute describing threshold value
        seed_number : int
            a seed makes output deterministic
        Returns
        -------
        graph_diffusion
            list of lists where each list contains nodes activated in one round of the diffusion process

        Raises
        ------
        Exception
            - given graph is not valid NetworkX graph
            - seed nodes are not present in the graph
            - wrong edge attribute name

    """
    impact_attr = 'impact'

    if type(graph) is nx.DiGraph or type(graph) is nx.Graph:
        # to_directed creates deep copy of graph when the graph is DiGraph
        graph = nx.to_directed(graph)
    else:
        raise Exception('Invalid graph format. Graph should be in NetworkX Graph or DiGraph format.')

    _valid_seed(graph, seed_nodes)
    nx.set_node_attributes(graph, name='is_active', values=False)
    _activate(graph, seed_nodes)

    if common_threshold is None:
        _init_threshold(graph, threshold_attr, seed_number)
    else:
        _set_single_threshold(graph, threshold_attr, common_threshold)

    _init_impact(graph, impact_attr)

    active_nodes = seed_nodes
    graph_diffusion = list()

    while active_nodes:
        graph_diffusion.append(active_nodes)
        nodes_to_activate = list()

        for node in active_nodes:
            for neighbor in graph.successors(node):
                if graph.node[neighbor]['is_active']:
                    continue

                influence_sum = 0
                for predecessor in graph.predecessors(neighbor):
                    if graph.node[neighbor]['is_active']:
                        influence_sum += graph[predecessor][neighbor][impact_attr]

                if influence_sum >= graph.node[neighbor][threshold_attr]:
                    graph.node[neighbor]['is_active'] = True
                    nodes_to_activate.append(neighbor)

        active_nodes = nodes_to_activate

    return graph_diffusion


# check if a sum of influences (weights of incoming edges) is always less or equal to 1 for every node
# ? check if graph has weight attribute describing diffusion prob.
def _valid_graph(graph, prob_attr):
    for node in graph.nodes:
        weight_sum = 0
        for predecessor in graph.predecessors(node):
            weight_sum += graph[predecessor][node][prob_attr]
        if weight_sum > 1:
            return False

    return True


def _init_impact(graph, impact_attr="weight"):
    for (u, v) in graph.edges():
        graph[u][v][impact_attr] = 1/graph.in_degree[v]


def _init_threshold(graph, threshold_attr, seed_number=None):
    random = Random(seed_number)
    for n in graph.nodes:
        if threshold_attr in graph.node[n]:
            if not 0 <= graph.node[n][threshold_attr] <= 1:
                raise Exception("Invalid node threshold value (outside [0;1] range)", graph.node[n][threshold_attr])
        else:
            graph.node[n][threshold_attr] = random.uniform(0, 1)


def _valid_seed(graph, seed):
    if not all(node in graph.nodes for node in seed):
        raise Exception("Invalid seed. There are nodes in seed which are not present among graph nodes.")


def _activate(graph, nodes_to_activate):
    for node in nodes_to_activate:
        graph.node[node]['is_active'] = True


def _set_single_threshold(graph, threshold_attr, threshold_value):
    if threshold_value > 1 or threshold_value < 0:
        raise Exception("Invalid node threshold value (outside [0;1] range)", threshold_value)

    for n in graph.nodes():
        graph.nodes[n][threshold_attr] = threshold_value
