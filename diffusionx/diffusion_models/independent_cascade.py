from random import Random

import networkx as nx


def spread(graph, seed_nodes, prob_attr="weight", common_prob=None, def_prob=0.4, seed_number=None):
    """Propagation diffusion in independent cascade model

        Simulate diffusion process in graph structure using independent cascade model
        starting from selected nodes (seed).

        Parameters
        ----------
        graph : networkx.Graph / networkx.DiGraph
            Network structure in NetworkX graph format
        seed_nodes : list of nodes
            Seed nodes, nodes initially active
        prob_attr : string
            name of edge attribute describing activation probability of an adjacent node
        common_prob : float
            if not None - activation probability set to an every edge
            if None (by default) - activation probability is read directly from the edge or if it is not present from
             the def_prob value
        def_prob : float
            default value of activation probability in case when there isn't any defined
        seed_number : int
            a seed makes output deterministic

        Returns
        -------
        graph_diffusion
            list of lists where each inner list contains nodes activated in one round of the diffusion process

        Raises
        ------
        Exception
            - given graph is not valid NetworkX graph
            - seed nodes are not present in the graph
            - wrong edge attribute name

    """

    random = Random(seed_number)

    if type(graph) is nx.DiGraph or type(graph) is nx.Graph:
        # to_directed creates deep copy of graph when the graph is DiGraph
        graph = nx.to_directed(graph)
    else:
        raise Exception('Invalid graph format. Graph should be in NetworkX Graph or DiGraph format.')

    _valid_seed(graph, seed_nodes)
    if common_prob is not None:
        _set_single_prob(graph, prob_attr, common_prob)
    else:
        _init_prob(graph, prob_attr, def_prob)

    nx.set_node_attributes(graph, name='is_active', values=False)
    _activate(graph, seed_nodes)

    active_nodes = seed_nodes
    graph_diffusion = list()

    while active_nodes:
        graph_diffusion.append(active_nodes)
        nodes_to_activate = list()
        for node in active_nodes:
            for neighbor in graph.neighbors(node):
                random_value = random.random()
                if not graph.node[neighbor]['is_active'] and random_value < graph[node][neighbor][prob_attr]:
                    graph.node[neighbor]['is_active'] = True
                    nodes_to_activate.append(neighbor)

        active_nodes = nodes_to_activate

    return graph_diffusion


def _init_prob(graph, prob_attr, def_prob):
    if def_prob > 1 or def_prob < 0:
        raise Exception("Invalid default edge activation probability value (outside [0;1] range)")

    for (u, v) in graph.edges():
        if prob_attr not in graph[u][v]:
            graph[u][v][prob_attr] = def_prob
        elif graph[u][v][prob_attr] > 1 or graph[u][v][prob_attr] < 0:
            raise Exception("Invalid edge activation probability value (outside [0;1] range)", graph[u][v][prob_attr])


def _set_single_prob(graph, prob_attr, prob):
    if prob > 1 or prob < 0:
        raise Exception("Invalid edge activation probability value (outside [0;1] range)", prob)

    for (u, v) in graph.edges():
        graph[u][v][prob_attr] = prob


def _valid_seed(graph, seed):
    if not all(node in graph.nodes for node in seed):
        raise Exception("Invalid seed. There are nodes in seed which are not present among graph nodes.")


def _activate(graph, nodes_to_activate):
    for node in nodes_to_activate:
        graph.node[node]['is_active'] = True
