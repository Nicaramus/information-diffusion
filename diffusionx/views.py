import json
import logging
import os.path
from tempfile import NamedTemporaryFile

from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.db.models import Count
from django.forms import ValidationError
from django.http import HttpResponseRedirect, Http404, JsonResponse, HttpResponse
from django.shortcuts import render, redirect
from django.urls import reverse
from django.utils.safestring import mark_safe

import diffusionx.experiment as exp
from diffusionx.diffusion_conf import network_types, seed_criteria
from diffusionx.file_handler import handle_csv_data
from diffusionx.konect_crawler import konect_network
from diffusionx.models import Network, Experiment, Diffusion
from information_diffusion.settings import MAX_GRAPH_UPLOAD_SIZE
from .forms import UserRegistrationForm, LoginForm


def index(request):
    if request.user.is_authenticated:
        networks_number = Network.objects.filter(user=request.user).count()
        experiments_number = Experiment.objects.filter(user=request.user).count()
    else:
        networks_number = 0
        experiments_number = 0

    return render(request, 'diffusionx/index.html',
                  {'networks_number': networks_number, 'experiments_number': experiments_number})


@login_required
def experiment(request):
    return redirect('/experiment/step/1')


@login_required
def step1(request):
    saved_networks = request.session.get('nt', None)

    select_networks = Network.objects. \
        values_list('id', 'name', 'nodes', 'edges', 'added_at', 'directed').filter(user=request.user)
    chosen_networks = []

    if saved_networks:
        saved_networks = list(map(int, saved_networks))
        chosen_networks = [x for x in select_networks if x[0] in saved_networks]
        select_networks = [x for x in select_networks if x[0] not in saved_networks]

    return render(request, 'diffusionx/experiments/steps/step_1.html',
                  {'select_list': select_networks, 'chosen_list': chosen_networks, 'step': 1})


@login_required
def step1_state(request):
    if request.is_ajax():
        if request.method == 'POST':
            chosen_networks = request.POST.getlist('networks[]')
            request.session['nt'] = chosen_networks
            return HttpResponse('Success')

    return HttpResponse(status=405)


@login_required
def step2(request):
    seed_selection_criteria = [(sc['id'], sc['name']) for sc in seed_criteria]
    return render(request, 'diffusionx/experiments/steps/step_2.html',
                  {'seed_criteria': seed_selection_criteria, 'step': 2})


@login_required
def step2_state(request):
    if request.is_ajax():
        if request.method == 'POST':
            json_data = json.loads(request.body)
            request.session['seed'] = json_data
            return HttpResponse('Success')
        elif request.method == 'GET':
            json_data = request.session.get('seed', [])
            return JsonResponse(json_data, safe=False)

    return HttpResponse(status=405)


@login_required
def step3(request):
    return render(request, 'diffusionx/experiments/steps/step_3.html', {'step': 3})


@login_required
def step3_state(request):
    if request.is_ajax():
        if request.method == 'POST':
            json_data = json.loads(request.body)
            request.session['models'] = json_data
            return HttpResponse('Success')
        elif request.method == 'GET':
            json_data = request.session.get('models', [])
            return JsonResponse(json_data, safe=False)

    return HttpResponse(status=405)


@login_required
def run_experiment(request):
    networks = request.session['nt']
    seed = request.session['seed']
    models = request.session['models']
    current_user = request.user

    if len(networks) == 0 or len(seed) == 0 or len(models) == 0:
        messages.error(request, 'Invalid data. Not all properties are specified')
        return redirect('diffusionx:experiment_new')

    exp.run_experiment(networks=networks, seed=seed, models=models, user=current_user)
    del request.session['nt']
    del request.session['seed']
    del request.session['models']
    messages.success(request, "Experiment  successfully completed")

    return redirect('diffusionx:experiment_list')


def login_view(request):
    form = LoginForm(request.POST or None)
    if request.POST and form.is_valid():
        user = form.login(request)
        if user:
            login(request, user)
            return HttpResponseRedirect("/")
        else:
            messages.error(request, "Bad credentials")

    if request.user.is_authenticated():
        return HttpResponseRedirect("/")
    else:
        return render(request, 'registration/login.html', {'login_form': form})


def logout_view(request):
    logout(request)
    return HttpResponseRedirect('/login')


def register(request):
    if request.method == 'POST':
        form = UserRegistrationForm(request.POST)
        if form.is_valid():
            user_obj = form.cleaned_data
            username = user_obj['username']
            email = user_obj['email']
            password = user_obj['password']
            bio = user_obj['bio']

            if User.objects.filter(username=username).exists():
                messages.error(request, 'Username is already taken.')
            elif User.objects.filter(email=email).exists():
                messages.error(request, 'Email address is already taken.')
            else:
                user = User.objects.create_user(username, email, password)
                user.refresh_from_db()
                user.profile.bio = bio
                user.save()
                user = authenticate(username=username, password=password)
                login(request, user)
                return HttpResponseRedirect('/')

    else:
        form = UserRegistrationForm()
    return render(request, 'registration/register.html', {'form': form})


@login_required
def network_graph(request, graph_type):
    type_id = int(graph_type)
    if 0 <= type_id <= len(network_types):
        form = network_types[type_id]['form']()
        return render(request, 'diffusionx/networks/network_graph.html', {'form': form})


def _save_network_graph(request, graph, graph_name):
    network = Network()
    if graph.number_of_nodes():
        network.save_nx_graph(graph_data=graph, user=request.user,
                              network_name=graph_name)
        messages.success(request, "Graph successfully added")
    else:
        messages.error(request, "Graph is empty")


@login_required
def network_new(request):
    if request.POST:
        network_selection = int(request.POST.get("network-select"))

        if network_selection == 1:
            try:
                upload = request.FILES["file"]
                extension = os.path.splitext(upload.name)[1].lower()
                if extension not in ['.csv', '.edgelist', '.mtx', '.edges', '.tsv']:
                    messages.error(request, 'The file type (%d) is not supported' % extension)
                    return HttpResponseRedirect(reverse("diffusionx:network_new"))

                delimiter = request.POST.get('delimiter', None)
                comments = request.POST.get('comments')
                directed = True if 'directed' in request.POST else False
                edge_type = request.POST.get('edgetype', 'none')

                # file cannot be too large
                if upload.size > MAX_GRAPH_UPLOAD_SIZE:
                    messages.error(request, "Uploaded file is too big (%.2f MB).Max. allowed size is 500MB."
                                   % (upload.size / (1000 * 1000),))
                    return HttpResponseRedirect(reverse("diffusionx:network_new"))

                f = NamedTemporaryFile(delete=False)
                path = f.name
                for chunk in upload.chunks():
                    f.write(chunk)
                f.close()

                if comments:
                    graph = handle_csv_data(path, delimiter=delimiter, comments=comments, directed=directed,
                                            edge_type=edge_type)
                else:
                    graph = handle_csv_data(path, delimiter=delimiter, directed=directed,
                                            edge_type=edge_type)

                _save_network_graph(request, graph, graph_name=request.POST['network_name'])
                os.unlink(path)
                return redirect('diffusionx:network_list')
            except Exception as e:
                logging.getLogger("error_logger").error("Unable to upload file. " + repr(e))
                messages.error(request, "Unable to upload file. " + repr(e))
        elif network_selection == 2:
            graph_model = int(request.POST['random-graph'])
            mutable_dict = request.POST.copy()
            graph_name = mutable_dict.pop('network_name')[0]
            graph_form = network_types[graph_model]['form']

            form = graph_form(mutable_dict)
            if form.is_valid():
                try:
                    graph = form.get_network()
                    _save_network_graph(request, graph, graph_name)
                    return redirect('diffusionx:network_list')
                except ValidationError as error:
                    messages.error(request, error.message)
            else:
                messages.error(request, "Invalid form")
        elif network_selection == 3:
            konect_url = request.POST['konect-url']
            try:
                graph = konect_network(konect_url)
                _save_network_graph(request, graph, graph_name=request.POST['network_name'])
                return redirect('diffusionx:network_list')
            except Exception as e:
                messages.error(request, e)

    network_type_names = [nt['description'] for nt in network_types]
    return render(request, 'diffusionx/networks/network_new.html', {'networks': network_type_names})


@login_required
def network_list(request):
    networks = Network.objects. \
        values_list('id', 'name', 'nodes', 'edges', 'added_at', 'directed').filter(user=request.user)
    data = {'object_list': networks}
    return render(request, 'diffusionx/networks/network_list.html', data)


@login_required
def network_detail(request, pk):
    try:
        network = Network.objects.get(user=request.user, id=pk)
    except Network.DoesNotExist:
        raise Http404("Network does not exist")

    return render(request, 'diffusionx/networks/network_detail.html', context={'network': network, })


@login_required
def get_json_graph(request, pk):
    try:
        json_graph = Network.objects.get(user=request.user, id=pk).graph
    except Network.DoesNotExist:
        raise Http404("Network does not exist")
    return JsonResponse(json_graph)


@login_required
def experiment_list(request):
    experiments = Experiment.objects.annotate(Count('diffusion')) \
        .values_list('id', 'started_at', 'diffusion__count').filter(user=request.user)
    data = {'object_list': experiments}
    return render(request, 'diffusionx/experiments/experiment_list.html', data)


@login_required
def experiment_detail(request, pk):
    experiment_object = Experiment.objects.get(user=request.user, id=pk)
    diffusions = Diffusion.objects.filter(experiment=experiment_object)
    data = {'experiment_id': experiment_object.id, 'started_at': experiment_object.started_at,
            'object_list': diffusions}
    return render(request, 'diffusionx/experiments/experiment_detail.html', data)


@login_required
def diffusion(request, pk):
    diffusion_object = Diffusion.objects.get(id=pk, experiment__user=request.user)
    steps = ['new active nodes']
    steps_sum = ['all active nodes']
    steps_accumulator = 0

    for step in json.loads(diffusion_object.diffusion_steps):
        steps.append(len(step))
        steps_accumulator += len(step)
        steps_sum.append(steps_accumulator)
    data = {'data': diffusion_object, 'steps': mark_safe(steps), 'steps_sum': mark_safe(steps_sum),
            'active_nodes': steps_accumulator, 'nonactive_nodes': diffusion_object.network.nodes - steps_accumulator}
    return render(request, 'diffusionx/experiments/diffusion.html', data)


@login_required
def network_delete(request, pk):
    try:
        network = Network.objects.get(user=request.user, id=pk)
        network.delete()
        messages.success(request, "Network successfully deleted")
    except Network.DoesNotExist:
        raise Http404("Network does not exist")

    return redirect('diffusionx:network_list')


@login_required
def experiment_delete(request, pk):
    experiment_object = Experiment.objects.get(user=request.user, id=pk)
    if request.method == 'POST':
        experiment_object.delete()
        messages.success(request, "Experiment successfully deleted")
    return redirect('diffusionx:experiment_list')


@login_required
def diffusion_delete(request, pk):
    diffusion_object = Diffusion.objects.get(id=pk)
    if diffusion_object.experiment.user == request.user:
        if request.method == 'POST':
            diffusion_object.delete()
            messages.success(request, "Diffusion successfully deleted")

    if diffusion_object.experiment.diffusion_set.count():
        return redirect('diffusionx:experiment_detail', pk=diffusion_object.experiment_id)
    else:
        return redirect('diffusionx:experiment_list')


@login_required
def get_json_diffusion(request, pk):
    try:
        experiment_id = Diffusion.objects.get(id=pk).experiment_id
        experiment_object = Experiment.objects.get(user=request.user, id=experiment_id)
        json_diffusion = Diffusion.objects.get(experiment=experiment_object, id=pk).diffusion_steps
    except Network.DoesNotExist:
        raise Http404("Diffusion does not exist")
    return JsonResponse(json_diffusion, safe=False)
