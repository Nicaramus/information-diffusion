import json
import numpy as np
import networkx as nx
from django.utils import timezone
from diffusionx.diffusion_conf import seed_criteria
from diffusionx.diffusion_models import independent_cascade, linear_threshold
from diffusionx.models import Network, Diffusion, Experiment


def run_experiment(networks=None, seed=None, models=None, user=None):
    experiment = Experiment(user=user, started_at=timezone.now())
    experiment.save()

    for network_id in networks:
        network = Network.objects.get(user=user, id=network_id)
        graph = network.get_nx_graph()

        for s in seed:
            seed_value = int(s['value'])
            selection_method = s['seed_criteria']
            selection_method_str = get_seed_criteria_dict(selection_method)['name']

            # nodes_number, seed_value_str = get_nodes_number(network, seed_value, s['value_type'])
            seed_nodes, seed_value_str = select_seed(graph, selection_method, seed_value, s['value_type'])

            if len(seed_nodes) == 0:
                continue

            for m in models:
                model = m['model']
                option = m['option']
                parameters = m['value']

                if option == 'single':
                    param = float(parameters)
                    diffusion_results = run_diffusion(graph, model, seed_nodes, param)
                    diffusion_instance = Diffusion(
                        network=network,
                        experiment=experiment,
                        model=model,
                        model_param=param,
                        steps=len(diffusion_results),
                        cover=count_cover(diffusion_results, graph.number_of_nodes()),
                        seed_criteria=selection_method_str,
                        seed_value=seed_value_str,
                        diffusion_steps=json.dumps(diffusion_results)
                    )
                    diffusion_instance.save()
                elif option == 'series':
                    from_value = float(parameters['from'])
                    to_value = float(parameters['to'])
                    step_value = int(parameters['step'])
                    param_series = np.linspace(from_value, to_value, num=step_value)
                    for param in param_series:
                        diffusion_results = run_diffusion(graph, model, seed_nodes, param)
                        diffusion_instance = Diffusion(
                            network=network,
                            experiment=experiment,
                            model=model,
                            model_param=param,
                            steps=len(diffusion_results),
                            cover=count_cover(diffusion_results, graph.number_of_nodes()),
                            seed_criteria=selection_method_str,
                            seed_value=seed_value_str,
                            diffusion_steps=json.dumps(diffusion_results)
                        )
                        diffusion_instance.save()


# get 'nodes_number' nodes from 'network' using criteria identified with 'selection_id'
def select_seed(network, selection_id, seed_value, value_type):
    nodes_number, value_str = get_nodes_number(network, seed_value, value_type)
    seed_nodes = select_seed_nodes_by_number(network, selection_id, nodes_number)
    return seed_nodes, value_str


# get 'nodes_number' nodes from 'network' using criteria identified with 'selection_id'
def select_seed_nodes_by_number(network, selection_id, nodes_number):
    if nodes_number < 0:
        raise ValueError('The nodes number cant be negative value')

    if nodes_number >= nx.number_of_nodes(network):
        return list(network.nodes())
    else:
        selection_dict = get_seed_criteria_dict(selection_id)
        return selection_dict['function'](network, nodes_number)


def get_seed_criteria_dict(criteria_id):
    return next((s for s in seed_criteria if s["id"] == criteria_id))


def count_cover(diffusion_steps, all_nodes_number):
    count_active = sum(len(step) for step in diffusion_steps)
    return count_active / all_nodes_number


def run_diffusion(network, model, seed, parameter):
    diffusion_steps = None
    if model == "icm":
        diffusion_steps = independent_cascade.spread(network, seed, common_prob=parameter)
    elif model == "ltm":
        diffusion_steps = linear_threshold.spread(network, seed, common_threshold=parameter)

    return diffusion_steps


def get_nodes_number(network, value, value_type):
    if value < 0:
        raise ValueError('The nodes number cant be negative value')

    number_of_all_nodes = network.number_of_nodes()
    if value_type == 'number':
        return value if number_of_all_nodes >= value else number_of_all_nodes, str(value)
    elif value_type == 'percentage':
        if value > 100:
            raise ValueError("You can't select more than all nodes")
        value_str = str(value) + '%'
        nodes_number = value * number_of_all_nodes / 100.0
        nodes_number = int(round(nodes_number))
        return nodes_number, value_str
