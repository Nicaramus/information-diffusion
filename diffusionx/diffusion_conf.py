from diffusionx.seed_selection import random_nodes, highest_degree, highest_betweenness, highest_closeness
from .forms import ErdosRenyiGraphForm, BarabasiAlbertGraphForm, NewmanWattsStrogatzGraphForm, WattsStrogatzGraphForm, \
    RandomRegularGraphForm


# graph generators and their forms
network_types = [
    {'form': BarabasiAlbertGraphForm, 'description': 'Barabási–Albert preferential attachment model'},
    {'form': ErdosRenyiGraphForm, 'description': 'Erdős-Rényi graph'},
    {'form': NewmanWattsStrogatzGraphForm, 'description': 'Newman–Watts–Strogatz small-world graph'},
    {'form': WattsStrogatzGraphForm, 'description': 'Watts–Strogatz small-world graph'},
    {'form': RandomRegularGraphForm, 'description': 'A random d-regular graph on n nodes'}
]

# seed selection criterias
seed_criteria = [
    {'id': 'random', 'function': random_nodes, 'name': 'Random nodes selection'},
    {'id': 'degree', 'function': highest_degree, 'name': 'Highest degree'},
    {'id': 'betweenness', 'function': highest_betweenness, 'name': 'Highest betweenness'},
    {'id': 'closeness', 'function': highest_closeness, 'name': 'Highest closeness'},
]

