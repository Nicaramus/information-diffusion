from django.apps import AppConfig


class DiffusionxConfig(AppConfig):
    name = 'diffusionx'
