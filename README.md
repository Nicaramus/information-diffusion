# ![alt text](diffusionx/static/img/favicon.png) DiffusionX

Diffusionx is a Django app for simulating information diffusion using Linear Threshold Model 
and Independent Cascade Model.

## Getting Started

### Prerequisites

Before you start you need to install:
- postgresql (>=9.4 required)
- conda (>=4.5.5 recommended)

Conda is package managment system, designed to build and manage software. 
The easiest way to get conda is to install Miniconda or Anaconda. 
More about it [here] (https://conda.io/docs/user-guide/install/index.html).

If you are not experienced with Django and PostgreSQL, use default settings while installing PostgreSQL.

### Installing

Open anaconda prompt console and move to project directory. 
Then create execution environment for the django app with command:
```
conda env create -f environment.yml
```
When environment is successfully created and dependencies are installed, activate the environment.
```
conda activate diffusionx
```
Next, install Bower using Node Package Manager.
```
npm install
```
Then install frontend packages:
```
npm run browser install 
```
Go to the `information_diffusion` folder and rename file `secrets.py.template` to `secrets.py`.
If you deploy app on the server you should change secret key and the database settings for security reasons.
In other cases using default values should be fine.

Initialize database tables by running migrations
```
python manage.py migrate
```

## Run server

Run developer server with: 
```
python manage.py runserver
```

You can use different ports and addresses [[more here]](https://docs.djangoproject.com/en/2.0/ref/django-admin/#runserver)
but by default the application is available at ``http://127.0.0.1:8000/``

You stop the server with CTRL-C keys combination.

## Create admin account
You can create a superuser account which allow to manage all users and saved data.
Type
``
python manage.py createsuperuser
``
and follow the instructions.

You can open admin panel at the `/admin/` directory of your domain. For example: `http://127.0.0.1:8000/admin/`

## License

This project is licensed under the GNU GPLv3 License - see the [LICENSE](LICENSE) file for details
